/** Bài 1:
 * Input: nhập vào số ngày làm việc : 28
 * 
 * 
 * Step:
 *  + Step1: tạo 1 biến chứa ngày làm việc
 *  + Step2: tạo biến chứa kết quả cần tìm
 *  + Step3: áp dụng công thức tiền lương = 100000(lương 1 ngày) * số ngày làm việc
 * 
 * 
 * Output: 2800000
 */

var sonNgay = 28;
var tienLuong = null;
tienLuong = 100000 * sonNgay;
console.log('tienLuong: ', tienLuong);


/** Bài 2:
 * Input: nhập vào 5 số thực
 * 
 * 
 * Step:
 *  + Step1: tạo 5 biến chứa 5 số thực: 5 10 15 20 25
 *  + Step2: tạo biến chứa kết quả cần tìm
 *  + Step3: áp dụng công thức giá trị trung bình = tổng 5 số đó chia cho 5
 * 
 * 
 * Output: 15
 */

var number1 = 5;
var number2 = 10;
var number3 = 15;
var number4 = 20;
var number5 = 25;
var trungBinh = null;
trungBinh = (number1 + number2 + number3 + number4 + number5) / 5;
console.log('trungBinh: ', trungBinh);


/** Baì 3:
 * Input : Nhập vào số tiền USD cần đổi 42 USD
 * 
 * Step:
 * Step1: tạo biến chứa số tiền USD
 * Step2: tạo biến chứa kết quả cần tìm
 * Step3: áp dụng công thức số tiền = 23.500(giá 1 USD) * số tiền USD
 * 
 * 
 * Output: 987000
 */

var money = 42;
var VND = null;
VND = 23500 * money;
console.log('VND: ', VND);


/** Bài 4:
 * Input: Nhập vào chiều dài và chiều rộng hình chữ nhật: 10 18
 * 
 * Step:
 * Step1: tạo 2 biến chứa chiều dài và chiều rộng
 * Step2: tạo 2 biến chứa chu vi và diện tích cần tìm
 * Step3: áp dụng công thức chu vi = (dài + rộng) *2;
 *                          diện tích = dài * rộng;
 * 
 * Output: chuVi = 56;
 *         dienTich = 180;
 */

var chieuDai = 10;
var chieuRong = 18;
var chuVi = null;
var dienTich = null;
chuVi = (chieuDai + chieuRong) * 2;
console.log('chuVi: ', chuVi);
dienTich = chieuDai * chieuRong;
console.log('dienTich: ', dienTich);


/** Baì 5:
 * Input: Nhập vào 2 ký số: 55
 * 
 * Step:
 * Step1: tạo biến chứa 1 số gồm 2 chữ số
 * Step2: tạo 2 biến chứa số hàng chục và hàng đơn vị
 * Step3: tạo biến tính tổng 2 số đó
 * 
 * 
 *Output: 10
 */

 var numBer1 = 55;
 var donVi = numBer1 % 10;
 var chuc = Math.floor(numBer1 / 10) % 10;
 var tong = donVi + chuc;
 console.log('tong: ', tong);