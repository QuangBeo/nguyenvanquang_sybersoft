
var arrNumber = [];
function nhapNumber(){
    document.querySelector("#hienMang").style.display = "block";
    var txtNumber = document.querySelector('#txt-number').value*1;
    arrNumber.push(txtNumber);
    document.querySelector('#txt-number').value = "";
    document.querySelector("#mang").innerHTML = arrNumber;

    //Tính tổng
    var sum = 0;
    for(var i = 0; i < arrNumber.length; i++){
        sum += arrNumber[i];
    }
    document.querySelector("#sum").innerHTML = sum;

    //Đếm số dương
    var dem = 0;
    for(var i = 0; i < arrNumber.length; i++){
        if(arrNumber[i] > 0){
            dem++;
        }
    }
    document.querySelector("#dem").innerHTML = dem;

    //tìm số nhỏ nhất
    var min = arrNumber[0];
    for(var i = 0; i < arrNumber.length; i++){
        if(min > arrNumber[i]){
            min = arrNumber[i];
        }
    }
    document.querySelector("#minNumber").innerHTML = min;
    
    //Tìm số dương nhỏ nhất trong mảng
    var minDuong = arrNumber[0];
    for(var i = 0; i < arrNumber.length; i++){
        if(minDuong > arrNumber[i] && arrNumber[i] >= 0){
            minDuong = arrNumber[i];
        }
    }
    document.querySelector("#minDuong").innerHTML = minDuong;

    //Tìm số chẵn cuối cùng
    function chancuoi () {
        let textShow;
        var newArray = arrNumber;
        for(var i = 0; i < newArray.length; i++){
            if(arrNumber[i] % 2 == 0){
                textShow = arrNumber[i];
            }
            if(textShow == undefined || null){
               textShow = -1; 
            }
        
        }
        return textShow;
    }
    document.querySelector("#timChanCuoi").innerHTML = chancuoi();

    //Sắp xếp tăng dần
    function tangdan() {
        return  arrNumber.sort(function(a, b){return a - b});

    }
    document.querySelector("#sapXepTang").innerHTML = tangdan();

    //Tìm số nguyên tố đầu tiên
    function timSoNguyenToDauTien(){
        let soNguyenTo;
        for(var i = 0; i < arrNumber.length; i++){
            if(kiemTraSoNguyenTo(arrNumber[i]) == true){
                soNguyenTo = arrNumber[i];
                console.log('soNguyenTo: ', soNguyenTo);
                break;
            }else{
                soNguyenTo = -1;
            }
        }
        return soNguyenTo;
    }
    document.querySelector("#soNguyenToDau").innerHTML = timSoNguyenToDauTien();
    
}


//Kiểm tra số nguyên tố
function kiemTraSoNguyenTo(number){
    let flag = true;
    if(number < 2){
        flag = false;
    }else {
        for(var i = 2; i < number - 1 ; i++){
            if(number % i == 0){
                flag = false;
                break;
            }
        }
    }
    return flag;
}

//Hoán đổi vị trí
function hoanDoiViTri(){
    var txtNumber1 = document.querySelector('#txt-number-1').value*1;
    var txtNumber2 = document.querySelector('#txt-number-2').value*1;
    var c = arrNumber[txtNumber1];
    arrNumber[txtNumber1] = arrNumber[txtNumber2];
    arrNumber[txtNumber2] = c;
    document.querySelector("#hoanDoiViTri").innerHTML = arrNumber;
}

//Tìm số nguyên
var arrSoThuc = [];
function timSoNguyen(){
    var dem = 0;
    var soThuc = document.getElementById("soThuc").value*1;
    arrSoThuc.push(soThuc);
    document.getElementById("soThuc").value = "";
    document.querySelector("#mangSoThuc").innerHTML = arrSoThuc;
    
    for(var i = 0; i < arrSoThuc.length; i++){
        if(Number.isInteger(arrSoThuc[i]) == true ){
            dem++;
        }
    }
    document.querySelector("#soNguyen").innerHTML = dem;


    //So sánh số âm và số dương
    var demAm = 0;
    var demDuong = 0;
    for(var i = 0; i < arrSoThuc.length; i++){
        if(arrSoThuc[i] < 0) {
            demAm++;
        }
        if(arrSoThuc[i] > 0){
            demDuong++;
        }
    }
    if(demAm > demDuong){
        document.querySelector("#soSanh").innerHTML = "Số âm lớn hơn số dương";
    }else if(demAm < demDuong){
        document.querySelector("#soSanh").innerHTML = "Số âm nhỏ hơn số dương";
    }else{
        document.querySelector("#soSanh").innerHTML = "Số dương bằng số âm";
    }
}