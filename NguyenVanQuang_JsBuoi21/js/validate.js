function kiemTraTrung(id,dsnv){
    var index = timKiem(id,dsnv);
    if(index !== -1){
        showMessage("tbTKNV", "Tài khoản nhân viên đã tồn tại.");
        return false;
    }else{
        showMessage("tbTKNV","");
        return true;
    }
}
//kiểm tra số 
function kiemTraSo(value,idErr,message){
    var reg = /^[0-9]{4,6}$/;
    var isNumber = reg.test(value);
    if(isNumber){
        showMessage(idErr,"");
        return true;
    }else{
        showMessage(idErr,message);
        return false;
    }
}
//kiểm tra tên
function kiemTraName(value,idErr,message){
    var reg =  /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]/;
    var isLetter = reg.test(value);
    if(isLetter){
        showMessage(idErr,"");
        return true;
    }else{
        showMessage(idErr,message);
        return false;
    }
}
//kiểm tra rỗng
function kiemTraRong(userInput,idErr,message){
    if(userInput.length == 0){
        showMessage(idErr,message);
        return false;
    }else{
        showMessage(idErr,"");
        return true;
    }
}
//Kiểm tra email
function kiemTraEmail(value,idErr){
    var reg = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let isEmail = reg.test(value);
    if(isEmail){
        showMessage(idErr,"");
        return true;
    }else{
        showMessage(idErr,"Nhập đúng định dạng email");
        return false;
    }
}
//Kiểm tra password
function kiemTraPass(value,idErr){
    // var reg = /^(?=.*\d)(?=.*[A-Z])[0-9a-zA-Z]{6,10}$/;
    var reg = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{6,10}$/;
    let isPass = reg.test(value);
    if(isPass){
        showMessage(idErr,"");
        return true;
    }else{
        showMessage(idErr,"Mật khẩu chứa từ 6 đến 10 ký tự và phải chứa 1 chữ hoa 1 số và 1 ký tự đặc biệt");
        return false;
    }
}
//Kiểm tra ngày
function kiemTraNgay(value,idErr){
    var reg = /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g;
    let isNgay = reg.test(value);
    if(isNgay){
        showMessage(idErr,"");
        return true;
    }else{
        showMessage(idErr,"Nhập đúng định dạng dd/mm/yyyy");
        return false;
    }
}
//Kiểm tra Lương
function kiemTraLuong(value,idErr){
    var firstNumber = 1000000;
    var lastNumber = 20000000;
    var reg = value;
    if(firstNumber <= reg && reg <= lastNumber){
        showMessage(idErr,"");
        return true;
    }else{
        showMessage(idErr,"Lương từ 1.000.000 đến 20.000.000");
        return false;
    }
}
//Kiểm tra chức vụ
function kiemTraChucVu(value,idErr){
    if(value=="Sếp" || value=="Trưởng phòng" || value=="Nhân viên"){
        showMessage(idErr,"");
        return true;
    }else{
        showMessage(idErr,"Chọn chức vụ");
        return false;
    }
}
//Kiểm tra Giờ làm
function kiemTraGioLam(value,idErr){
    var firstNumber = 80;
    var lastNumber = 200;
    var reg = value;
    if(firstNumber <= reg && reg <= lastNumber){
        showMessage(idErr,"");
        return true;
    }else{
        showMessage(idErr,"Nhập giờ làm từ 80 đến 200");
        return false;
    }
}
