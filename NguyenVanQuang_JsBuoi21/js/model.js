
function NhanVien(taiKhoan,ten,email,matKhau,ngayLam,luongCB,chucVu,gioLam){
    this.taikhoan = taiKhoan;
    this.ten = ten;
    this.email = email;
    this.matkhau = matKhau;
    this.ngaylam = ngayLam;
    this.luongcb = luongCB;
    this.chucvu = chucVu;
    this.giolam = gioLam;
    this.tongluong = function(){
        if(this.chucvu=="Sếp"){
            return this.luongcb * 3;
        }
        else if(this.chucvu=="Trưởng phòng"){
            return this.luongcb * 2;
        }
        else if(this.chucvu=="Nhân viên"){
            return this.luongcb * 1;
        }
    };
    this.xeploai = function(){
        if(this.giolam >=192){
            return "Xuất sắc";
        } else if(this.giolam >= 176){
            return "Giỏi";
        } else if(this.giolam >= 160){
            return "Khá";
        } else {
            return "Trung bình";
        }
    }
}

