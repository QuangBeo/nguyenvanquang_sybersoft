function LayThongTinTuForm(){

    var taiKhoan = document.getElementById('tknv').value;
    var ten = document.getElementById('name').value;
    var email = document.getElementById('email').value;
    var matKhau = document.getElementById('password').value;
    var ngayLam = document.getElementById('datepicker').value;
    var luongCB = document.getElementById('luongCB').value;
    var chucVu = document.getElementById('chucvu').value;
    var gioLam = document.getElementById('gioLam').value;

    //Tạo nhân viên
    var nhanVien = new NhanVien(taiKhoan,ten,email, matKhau,ngayLam,luongCB,chucVu,gioLam);
    return nhanVien;
}

function renderDanhSachNhanVien(nvArr){
    var contentHTML = "";
    for(var i = 0; i < nvArr.length; i++){
        var currentNV = nvArr[i];
        var contentTr = 
        `
            <tr>
                <td>${currentNV.taikhoan}</td>
                <td>${currentNV.ten}</td>
                <td>${currentNV.email}</td>
                <td>${currentNV.ngaylam}</td>
                <td>${currentNV.chucvu}</td>
                <td>${currentNV.tongluong()}</td>
                <td>${currentNV.xeploai()}</td>
                <td>
                    <button class="btn btn-danger" onclick="xoaNV('${currentNV.taikhoan}')">Xoá</button>
                    <button class="btn btn-primary" id="sua" onclick="suaNhanVien('${currentNV.taikhoan}')">Sửa</button>
                </td>
            </tr>
        `;
        contentHTML += contentTr;
    }
    document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function showThongTinLenForm(nv){
    document.getElementById('tknv').value = nv.taikhoan;
    document.getElementById('name').value = nv.ten;
    document.getElementById('email').value = nv.email;
    document.getElementById('password').value = nv.matkhau;
    document.getElementById('datepicker').value = nv.ngaylam;
    document.getElementById('luongCB').value = nv.luongcb;
    document.getElementById('chucvu').value = nv.chucvu;
    document.getElementById('gioLam').value = nv.giolam;
}

function resetForm(){
    document.getElementById("form").reset();
}

function showMessage(idErr,message){
    document.getElementById(idErr).innerHTML = message;
    document.getElementById(idErr).style.display = "block";
}
