const DSNV = "DSNV";
var dsnv = [];

function luuLocalStorage(){
    var jssonDsnv = JSON.stringify(dsnv);
    localStorage.setItem(DSNV,jssonDsnv);
}

var dataJson = localStorage.getItem(DSNV);
if(dataJson !== null){
    var nvArr = JSON.parse(dataJson);
    for(var i = 0; i < nvArr.length; i++){
        var item = nvArr[i];
        var nv = new NhanVien(item.taikhoan,item.ten,item.email, item.matkhau,item.ngaylam,item.luongcb,item.chucvu,item.giolam);
        dsnv.push(nv);
    }
    renderDanhSachNhanVien(dsnv);
}

//Thêm nhân viên
function themNhanVien(){
    var nv = LayThongTinTuForm();
    
    var isValid = true;
    //Kiểm tra tài khoản
    isValid = kiemTraTrung(nv.taikhoan,dsnv)&&
    kiemTraRong(nv.taikhoan, "tbTKNV", "Mã sinh viên không được rỗng!!!") &&
    kiemTraSo(nv.taikhoan,"tbTKNV","Tài khoản phải là số, từ 4 - 6 ký tự");
    // Kiểm tra tên nhân viên
    isValid = isValid &
    kiemTraRong(nv.ten,"tbTen","Hãy nhập họ và tên") &&
    kiemTraName(nv.ten,"tbTen","Tên phải là chữ");
    //kiểm tra email
    isValid = isValid &
    kiemTraRong(nv.email,"tbEmail","Hãy nhập email") &&
    kiemTraEmail(nv.email, "tbEmail");
    //Kiểm tra mật khẩu
    isValid = isValid &
    kiemTraRong(nv.matkhau,"tbMatKhau","Hãy nhập mật khẩu") &&
    kiemTraPass(nv.matkhau,"tbMatKhau");
    //Kiểm tra ngày làm
    isValid = isValid &
    kiemTraRong(nv.ngaylam,"tbNgay","Hãy nhập ngày làm") &&
    kiemTraNgay(nv.ngaylam,"tbNgay");
    //Kiểm tra lương cb
    isValid = isValid &
    kiemTraRong(nv.luongcb,"tbLuongCB","Hãy nhập lương cơ bản") &&
    kiemTraLuong(nv.luongcb,"tbLuongCB");
     //Kiểm tra chức vụ
    isValid = isValid &
    kiemTraRong(nv.chucvu,"tbChucVu","Hãy nhập chức vụ") && 
    kiemTraChucVu(nv.chucvu,"tbChucVu");
    //Kiểm tra giờ làm
    isValid = isValid &
    kiemTraRong(nv.giolam,"tbGiolam","Hãy nhập giờ làm") &&
    kiemTraGioLam(nv.giolam,"tbGiolam");
    if(isValid){
        showMessage("thongBao","Đã thêm thành công nhân viên");
        dsnv.push(nv);
        luuLocalStorage();
        renderDanhSachNhanVien(dsnv);
    }
}

//Xoá nhân viên
function xoaNV(id){
    var vitri = timKiem(id,dsnv);
    if(vitri != -1){
        dsnv.splice(vitri,1);
        luuLocalStorage();
        renderDanhSachNhanVien(dsnv);
    }
}

//sửa nhân viên
function suaNhanVien(taikhoan){
    
    document.getElementById('myModal').style.display = "block";
    document.getElementById('myModal').classList.add("show");

    var vitri = timKiem(taikhoan, dsnv);
    if(vitri == -1) return;
    var data = dsnv[vitri];
    showThongTinLenForm(data);
    document.getElementById("tknv").disabled = true;
}
function dong(){
    document.getElementById('myModal').style.display = "none";
    document.getElementById('myModal').classList.remove("show");
}

//cập nhật thông tin nhân viên
function capNhatNV(){
    var data = LayThongTinTuForm();
    var vitri = timKiem(data.taikhoan, dsnv);
    if(vitri == -1) return;
    dsnv[vitri] = data;

    var isValid = true;
    //Kiểm tra tài khoản
    isValid = kiemTraRong(nv.taikhoan, "tbTKNV", "Mã sinh viên không được rỗng!!!") &&
    kiemTraSo(nv.taikhoan,"tbTKNV","Tài khoản phải là số, từ 4 - 6 ký tự");
    // Kiểm tra tên nhân viên
    isValid = isValid &
    kiemTraRong(data.ten,"tbTen","Hãy nhập họ và tên") &&
    kiemTraName(data.ten,"tbTen","Tên phải là chữ");
    //kiểm tra email
    isValid = isValid &
    kiemTraRong(data.email,"tbEmail","Hãy nhập email") &&
    kiemTraEmail(data.email, "tbEmail");
    //Kiểm tra mật khẩu
    isValid = isValid &
    kiemTraRong(data.matkhau,"tbMatKhau","Hãy nhập mật khẩu") &&
    kiemTraPass(data.matkhau,"tbMatKhau");
    //Kiểm tra ngày làm
    isValid = isValid &
    kiemTraRong(data.ngaylam,"tbNgay","Hãy nhập ngày làm") &&
    kiemTraNgay(data.ngaylam,"tbNgay");
    //Kiểm tra lương cb
    isValid = isValid &
    kiemTraRong(data.luongcb,"tbLuongCB","Hãy nhập lương cơ bản") &&
    kiemTraLuong(data.luongcb,"tbLuongCB");
    //Kiểm tra chức vụ
    isValid = isValid &
    kiemTraRong(data.chucvu,"tbChucVu","Hãy nhập chức vụ") && 
    kiemTraChucVu(data.chucvu,"tbChucVu");
    //Kiểm tra giờ làm
    isValid = isValid &
    kiemTraRong(data.giolam,"tbGiolam","Hãy nhập giờ làm") &&
    kiemTraGioLam(data.giolam,"tbGiolam");
    if(isValid){
        showMessage("thongBao","Đã cập nhật thành công nhân viên");
        renderDanhSachNhanVien(dsnv);
        luuLocalStorage();
        document.getElementById("tknv").disabled = false;
    }
}

//Tìm kiếm vị trí
function timKiem(id,nvArr){
    for(var i = 0; i < nvArr.length; i++){
        var item = nvArr[i];
        if(item.taikhoan == id){
            return i;
        }
    }
    return -1;
}

//Tìm kiếm nhân viên
function search(){
    var inputSearch = document.getElementById("searchName").value;
    var contentHTML = "";
    for(var i = 0; i < dsnv.length; i++){
        if(dsnv[i].xeploai() == inputSearch) {
            var currentNV = dsnv[i];
            var contentTr = 
            `
                <tr>
                    <td>${currentNV.taikhoan}</td>
                    <td>${currentNV.ten}</td>
                    <td>${currentNV.email}</td>
                    <td>${currentNV.ngaylam}</td>
                    <td>${currentNV.chucvu}</td>
                    <td>${currentNV.tongluong()}</td>
                    <td>${currentNV.xeploai()}</td>
                    <td>
                        <button class="btn btn-danger" onclick="xoaNV('${currentNV.taikhoan}')">Xoá</button>
                        <button class="btn btn-primary" id="sua" onclick="suaNhanVien('${currentNV.taikhoan}')">Sửa</button>
                    </td>
                </tr>
            `;
            contentHTML += contentTr;
        }
    }
    document.getElementById("tableDanhSach").innerHTML = contentHTML;
}
