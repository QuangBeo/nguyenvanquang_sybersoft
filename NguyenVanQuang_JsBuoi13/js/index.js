
// Bài 1
function tinhTienLuong(){
    var soNgay = document.getElementById("songay").value*1;
    var tienLuong = null;
    tienLuong = 100000 * soNgay;
    document.getElementById("tienLuong").innerHTML =
    // "Tiền lương của bạn tháng này là: <br>" +
    // tienLuong;
    ` Tiền lương của bạn tháng này là: <br> <h2>${tienLuong}</h2>`;
}

// Bài 2:
function tinhTrungBinh(){
    var number1 = document.getElementById("number1").value*1;
    var number2 = document.getElementById("number2").value*1;
    var number3 = document.getElementById("number3").value*1;
    var number4 = document.getElementById("number4").value*1;
    var number5 = document.getElementById("number5").value*1;
    var trungBinh = null;
    trungBinh = (number1 + number2 + number3 + number4 + number5) / 5;
    document.getElementById("trungBinh").innerHTML=
    ` Trung bình cộng của 5 số là: <br> <h2>${trungBinh}</h2>`;
}

// Bài 3:
function doiTien(){
    var money = document.getElementById("soTien").value*1;
    var tienViet = money * 23500;
    document.getElementById("tienViet").innerHTML=
    ` Đổi ra tiền Việt bằng: <br> <h2>${tienViet}</h2>`;
}

// Bài 4:
function tinh(){
    var chieuDai = document.getElementById("chieuDai").value*1;
    var chieuRong = document.getElementById("chieuRong").value*1;
    var chuVi = (chieuDai + chieuRong) * 2;
    var dienTich = chieuDai * chieuRong;
    document.getElementById("chuVi").innerHTML =
    ` Chu vi hình chữ nhật: <br> <h2>${chuVi}</h2>`; 
    document.getElementById("dienTich").innerHTML =
    ` Dien tich hình chữ nhật: <br> <h2>${dienTich}</h2>`; 
}

// Bài 5:
function tinhTong(){
    var number = document.getElementById("number").value*1;
    var donVi = (number % 10);
    var chuc = Math.floor(number / 10) % 10;
    var tong2So = donVi + chuc;
    document.getElementById("tong").innerHTML = 
    ` Tổng 2 chữ số: <br> <h2>${tong2So}</h2>`;
}