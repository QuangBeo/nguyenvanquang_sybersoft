

function tinhThue(){
    var hoTen = document.getElementById('hoTen').value;
    var thuNhap = document.getElementById('thuNhap').value*1;
    var soNguoi = document.getElementById('soNguoi').value*1;

    var tongThuNhapChiuThue = thuNhap - 4 - (soNguoi * 1.6);

    var tienThue;

    if(tongThuNhapChiuThue <= 60) {
        tienThue = tongThuNhapChiuThue * 0.05;
    }else if(tongThuNhapChiuThue <= 120) {
        tienThue = tongThuNhapChiuThue * 0.1;
    }else if(tongThuNhapChiuThue <= 210) {
        tienThue = tongThuNhapChiuThue * 0.15;
    }else if(tongThuNhapChiuThue <= 384) {
        tienThue = tongThuNhapChiuThue * 0.2;
    }else if(tongThuNhapChiuThue <= 624) {
        tienThue = tongThuNhapChiuThue * 0.25;
    }else if(tongThuNhapChiuThue <= 960) {
        tienThue = tongThuNhapChiuThue * 0.3;
    } else {
        tienThue = tongThuNhapChiuThue * 0.35;
    }

    document.getElementById("result").innerHTML = 
    `Tiền thuế phải chịu là: ${tienThue.toFixed(2)} Triệu`;
}