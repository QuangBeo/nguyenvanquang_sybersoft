

function hienSoKetNoi(){
    console.log("yes");
    var txtKhachHang = document.getElementById("khachHang").value*1;

    if(txtKhachHang == 2){
        document.getElementById("soKetNoi").style.display = "inline";
    } else {
        document.getElementById("soKetNoi").style.display = "none";
    }
}


function tinhTien(){
    var khachHang = document.getElementById('khachHang').value*1;
    var maKH = document.getElementById('maKH').value*1;
    var soKenh = document.getElementById('soKenh').value*1;
    var soKetNoi = document.getElementById('soKetNoi').value*1;

    var tienCap;

    if(khachHang == 1) {
        tienCap = 4.5 + 20.5 + soKenh * 7.5;
    } else if(khachHang == 2){
        if(soKetNoi <= 10) {
            tienCap = 15 + 75 + soKenh * 50;
        }else {
            tienCap = 15 + 75 + (soKetNoi - 10) * 5 + soKenh * 50;
        }
    }

    document.getElementById("result").innerHTML = 
    `Mã khách hàng ${maKH} tổng tiền cap la: ${tienCap}`;
}