/**
 * Input: Nhập vào 3 số nguyên
 * 
 * Step:
 * Step1: tạo ra 3 biến lấy 3 số nhập từ input
 * Step2: tạo 2 biến đếm lưu số chẵn lẻ
 * Step3: dùng thuật toán tăng biến đếm tương ứng
 * 
 * Output: In ra số chẵn số lẻ
 */


function Dem(){
    var num1 = document.getElementById('num1').value*1;
    var num2 = document.getElementById('num2').value*1;
    var num3 = document.getElementById('num3').value*1;

    var demChan = 0;
    var demLe = 0;

    if(num1 % 2 == 0){
        demChan = demChan + 1;
    }else{
        demLe = demLe + 1;
    }
    if(num2 % 2 == 0){
        demChan = demChan + 1;
    }else{
        demLe = demLe + 1;
    }
    if(num3 % 2 == 0){
        demChan = demChan + 1;
    }else{
        demLe = demLe + 1;
    }
    
    document.getElementById("result").innerHTML = 
    `Đếm số chẵn bằng ${demChan} <br> Đếm số lẻ bằng ${demLe}`;
}