/**
 * Input: Nhập vào ngày tháng năm
 * 
 * Step:
 * Step1: tạo 3 biến chứa 3 giá trị ngày tháng năm người dùng nhập vào
 * Step2: Áp dụng ngày tháng năm trong thực tế và ứng dụng thuật toán để tìm ra ngày tháng năm trước là sau
 * Step3: In ra ngày tháng năm đó
 * 
 * Output: In ra ngày tháng năm cần tìm
 */



// Hàm tìm ngày hôm qua
function timNgayHomQua(){
    var ngay = document.getElementById('ngay').value*1;
    var thang = document.getElementById('thang').value*1;
    var nam = document.getElementById('nam').value*1;

    if((nam % 4 == 0 && nam % 100 != 0) || nam % 400 == 0){
        if(thang == 5 || thang == 7 || thang == 10 || thang == 12) {
            if(ngay == 1){
                document.getElementById("result").innerHTML = 
                `Ngày 30 Tháng ${thang - 1} Năm ${nam}`;
            }else {
                document.getElementById("result").innerHTML = 
                `Ngày ${ngay - 1} Tháng ${thang} Năm ${nam}`;
            }
        }if(thang == 1 || thang == 2 || thang == 4 || thang == 6 || thang == 8 || thang == 9 || thang == 11) {
            if(ngay == 1 && thang != 1){
                document.getElementById("result").innerHTML = 
                `Ngày 31 Tháng ${thang - 1} Năm ${nam}`;
            }if(ngay == 1 && thang == 1) {
                document.getElementById("result").innerHTML = 
                `Ngày 31 Tháng 12 Năm ${nam - 1}`;
            }if(ngay != 1) {
                document.getElementById("result").innerHTML = 
                `Ngày ${ngay - 1} Tháng ${thang} Năm ${nam}`;
            }
        }if(thang == 3){
            if(ngay == 1){
                document.getElementById("result").innerHTML = 
                `Ngày 29 Tháng ${thang -1} Năm ${nam}`;
            }else{
                document.getElementById("result").innerHTML = 
                `Ngày ${ngay - 1} Tháng ${thang} Năm ${nam}`;
            }
        }
    }else{
        if(thang == 5 || thang == 7 || thang == 10 || thang == 12){
            if(ngay == 1){
                document.getElementById("result").innerHTML = 
                `Ngày 30 Tháng ${thang - 1} Năm ${nam}`;
            }else {
                document.getElementById("result").innerHTML = 
                `Ngày ${ngay - 1} Tháng ${thang} Năm ${nam}`;
            }
        }if(thang == 1 || thang == 2 || thang == 4 || thang == 6 || thang == 8 || thang == 9 || thang == 11){
            if(ngay == 1 && thang != 1){
                document.getElementById("result").innerHTML = 
                `Ngày 31 Tháng ${thang - 1} Năm ${nam}`;
            }if(ngay == 1 && thang == 1) {
                document.getElementById("result").innerHTML = 
                `Ngày 31 Tháng 12 Năm ${nam - 1}`;
            }if(ngay != 1) {
                document.getElementById("result").innerHTML = 
                `Ngày ${ngay - 1} Tháng ${thang} Năm ${nam}`;
            }
        }if(thang == 3){
            if(ngay == 1){
                document.getElementById("result").innerHTML = 
                `Ngày 28 Tháng ${thang -1} Năm ${nam}`;
            }else{
                document.getElementById("result").innerHTML = 
                `Ngày ${ngay - 1} Tháng ${thang} Năm ${nam}`;
            }
        }
    }
}


// Hàm tìm ngày mai
function timNgayMai(){
    var ngay = document.getElementById('ngay').value*1;
    var thang = document.getElementById('thang').value*1;
    var nam = document.getElementById('nam').value*1;

    if((nam % 4 == 0 && nam % 100 != 0) || nam % 400 == 0){
        if(thang == 1 || thang == 3 || thang == 5 || thang == 7 || thang == 8 || thang == 10 || thang == 12) {
            if(ngay == 31 && thang != 12){
                document.getElementById("result").innerHTML = 
                `Ngày 1 Tháng ${thang + 1} Năm ${nam}`;
            }else if(ngay == 31 && thang == 12){
                document.getElementById("result").innerHTML = 
                `Ngày 1 Tháng 1 Năm ${nam + 1}`;
            }else if(ngay != 31) {
                document.getElementById("result").innerHTML = 
                `Ngày ${ngay + 1} Tháng ${thang} Năm ${nam}`;
            }
        }else if(thang == 4 || thang == 6 || thang == 9 || thang == 11) {
            if(ngay == 30) {
                document.getElementById("result").innerHTML = 
                `Ngày 1 Tháng ${thang + 1} Năm ${nam}`;
            }else if(ngay != 30) {
                document.getElementById("result").innerHTML = 
                `Ngày ${ngay + 1} Tháng ${thang} Năm ${nam}`;
            }
        } else if(thang == 2){
            if(ngay == 29){
                document.getElementById("result").innerHTML = 
                `Ngày 1 Tháng ${thang + 1} Năm ${nam}`;
            }else if(ngay != 29){
                document.getElementById("result").innerHTML = 
                `Ngày ${ngay + 1} Tháng ${thang} Năm ${nam}`;
            }
        }
    }else{
        if(thang == 1 || thang == 3 || thang == 5 || thang == 7 || thang == 8 || thang == 10 || thang == 12) {
            if(ngay == 31 && thang != 12){
                document.getElementById("result").innerHTML = 
                `Ngày 1 Tháng ${thang + 1} Năm ${nam}`;
            }else if(ngay == 31 && thang == 12){
                document.getElementById("result").innerHTML = 
                `Ngày 1 Tháng 1 Năm ${nam + 1}`;
            }else if(ngay != 31) {
                document.getElementById("result").innerHTML = 
                `Ngày ${ngay + 1} Tháng ${thang} Năm ${nam}`;
            }
        }else if(thang == 4 || thang == 6 || thang == 9 || thang == 11) {
            if(ngay == 30) {
                document.getElementById("result").innerHTML = 
                `Ngày 1 Tháng ${thang + 1} Năm ${nam}`;
            }else if(ngay != 30) {
                document.getElementById("result").innerHTML = 
                `Ngày ${ngay + 1} Tháng ${thang} Năm ${nam}`;
            }
        } else if(thang == 2){
            if(ngay == 28){
                document.getElementById("result").innerHTML = 
                `Ngày 1 Tháng ${thang + 1} Năm ${nam}`;
            }else if(ngay != 28){
                document.getElementById("result").innerHTML = 
                `Ngày ${ngay + 1} Tháng ${thang} Năm ${nam}`;
            }
        }
    }
}