/**
 * Input: nhập vào 3 cạnh của tam giác
 * 
 * Step:
 * Step1: tạo 3 biến chứa 3 cạnh của tam giac
 * Step2: dùng thuật toán tìm ra tam giác hình gì
 * Step3: in ra hình tam giác đó
 * 
 * Output: xuất ra màn hình tam giác
 */

function Tim(){
    var canh1 = document.getElementById('canh1').value*1;
    var canh2 = document.getElementById('canh2').value*1;
    var canh3 = document.getElementById('canh3').value*1;
    // var canh11 = canh1 * canh1;
    // var canh22 = canh2 * canh2;
    // var canh33 = canh3 * canh3;
    // var dienTich1 = canh2 * canh2 + canh3 * canh3;
    // var dienTich2 = canh1 * canh1 + canh3 * canh3;
    // var dienTich3 = canh2 * canh2 + canh3 * canh3;
    
    if(canh1 == canh2 && canh1 == canh3) {
        document.getElementById("result").innerHTML =
        `Là tam giác đều`;
    }
    else if(canh1 == canh2 || canh1 == canh3){
        document.getElementById("result").innerHTML =
        `Là tam giác cân`;
    }
    // if(canh11 == dienTich1 || canh22 == dienTich2 || canh33 == dienTich3){
    //     document.getElementById("result").innerHTML =
    //     `Là tam giác vuông`;
    // }
    else if(canh1 * canh1 == canh2 * canh2 + canh3 * canh3 ||
       canh2 * canh2 == canh1 * canh1 + canh3 * canh3 ||
       canh3 * canh3 == canh1 * canh1 + canh2 * canh2){
        document.getElementById("result").innerHTML =
        `Là tam giác vuông`;
    } else {
        document.getElementById("result").innerHTML =
        `Là tam giác thường`;
    }
}