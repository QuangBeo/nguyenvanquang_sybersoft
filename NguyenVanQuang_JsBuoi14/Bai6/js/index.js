/**
 * Input: nhập vào tháng năm
 * 
 * 
 * Step:
 * Step1: tạo 2 biến chứa thông tin người dùng
 * Step2: tạo biến chứa số ngày cần tìm
 * Step3: in ra biến đó 
 *
 * 
 * Output: in ra số ngày trong tháng và năm đó
 */


function demNgay(){
    var thang = document.getElementById('thang').value*1;
    var nam = document.getElementById('nam').value*1;

    var demNgay = 0;

    if(thang == 1 || thang == 3 || thang == 5 || thang == 7 || thang == 8 || thang == 10 || thang == 12){
        document.getElementById("result").innerHTML =
        `Tháng ${thang} Năm ${nam} có 31 ngày`;
    }else if(thang == 4 || thang == 6 || thang == 9 || thang == 11){
        document.getElementById("result").innerHTML =
        `Tháng ${thang} Năm ${nam} có 30 ngày`;
    }else if(thang == 2){
        if(nam % 4 == 0 && nam % 100 != 0 || nam % 400 == 0){
            document.getElementById("result").innerHTML =
            `Tháng ${thang} Năm ${nam} có 29 ngày`;
        }else{
            document.getElementById("result").innerHTML =
            `Tháng ${thang} Năm ${nam} có 28 ngày`;
        }
    }
}