/**
 * Input: lựa chọn thành viên gia đình
 * 
 * Step:
 * Step1: viết chương trình lựa chọn thành viên
 * Step2: lấy giá trị đó từ lựa chọn của thành viên
 * Step3: in ra lời chào 
 * 
 * Output: in ra lời chào các thành viên tương ứng
 */

function loiChao(){
    var select = document.getElementById('select').value;

    if(select == "B"){
        document.getElementById("hello").innerHTML = 
        `Xin chào Bố`;
    }
    if(select == "M"){
        document.getElementById("hello").innerHTML = 
        `Xin chào Mẹ`;
    }
    if(select == "A"){
        document.getElementById("hello").innerHTML = 
        `Xin chào Anh Trai`;
    }
    if(select == "E"){
        document.getElementById("hello").innerHTML = 
        `Xin chào Em gái`;
    }
}