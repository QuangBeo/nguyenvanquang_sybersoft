/**
 * input: nhập vào thông tin tên và toạ độ của sinh viên
 * 
 * Step:
 * Step1: tạo các biến chứa thông tin của các sinh viên
 * Step2: tính quãng đường từ từng học sinh đến trường
 * Step3: tìm ra sinh viên nào xa trường nhất
 * 
 * 
 * Output: in ra học sinh xa trường nhất
 */


function timDuong(){
    var quang = document.getElementById("quang").value;
    var quangx = document.getElementById("quangx").value*1;
    var quangy = document.getElementById("quangy").value*1;
    var chien = document.getElementById("chien").value;
    var chienx = document.getElementById("chienx").value*1;
    var chieny = document.getElementById("chieny").value*1;
    var vinh = document.getElementById("vinh").value;
    var vinhx = document.getElementById("vinhx").value*1;
    var vinhy = document.getElementById("vinhy").value*1;
    var truongx = document.getElementById("truongx").value*1;
    var truongy = document.getElementById("truongy").value*1;

    var quangD = Math.sqrt(Math.pow(truongx - quangx,2) + Math.pow(truongy - quangy,2));
    var chienD = Math.sqrt(Math.pow(truongx - chienx,2) + Math.pow(truongy - chieny,2));
    var vinhD = Math.sqrt(Math.pow(truongx - vinhx,2) + Math.pow(truongy - vinhy,2));
    

    // var quangDuong = Math.max(quangD,chienD);
    // console.log('quangDuong: ', quangDuong);
    if(quangD == chienD && quangD == vinhD) {
        document.getElementById("result").innerHTML = 
        "Nhà của bạn xa nhất là: " + quang + " và " + chien + " và " + vinh;
    }else if(quangD == chienD && quangD > vinhD){
        document.getElementById("result").innerHTML = 
        "Nhà bạn xa nhất là: " + quang + " và " + chien;
    }else if(quangD == vinhD && quangD > chienD){
        document.getElementById("result").innerHTML = 
        "Nhà bạn xa nhất là: " + quang + " và " + vinh;
    }else if(chienD == vinhD && chienD > quangD){
        document.getElementById("result").innerHTML = 
        "Nhà bạn xa nhất là: " + chien + " và " + vinh;
    }else if(quangD >= chienD && quangD >= vinhD){
        document.getElementById("result").innerHTML = 
        "Nhà bạn xa nhất là: " + quang;
    } else if(chienD >= quangD && chienD >= vinhD){
        document.getElementById("result").innerHTML = 
        "Nhà bạn xa nhất là: " + chien;
    }else if(vinhD >= quangD && vinhD >= chienD){
        document.getElementById("result").innerHTML = 
        "Nhà bạn xa nhất là: " + vinh;
    }
}