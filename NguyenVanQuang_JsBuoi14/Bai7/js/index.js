/**
 * input: nhập vào số có 3 chữ số
 * 
 * 
 * Step:
 * Step1: tạo 1 biến chứa dữ liệu người dùng nhập vào
 * Step2: tạo biến chứa kết quả đọc số
 * Step3: dùng thuật toán tách 3 số đó
 * Step4: in ra cách đọc
 * 
 * Output: in ra số đó
 */

function doc(){
    var number = document.getElementById('number').value*1;

    var tram = Math.floor(number / 100) % 10;
    var chuc = Math.floor(number / 10) % 10;
    var donVi = number % 10;

    var text = ``;
    // Hàng trăm
    if(tram == 1){
        text += "Một";
    }else if(tram == 2){
        text += "Hai";
    }else if(tram == 3){
        text += "Ba";
    }else if(tram == 4){
        text += "Bốn";
    }else if(tram == 5){
        text += "Năm";
    }else if(tram == 6){
        text += "Sáu";
    }else if(tram == 7){
        text += "Bảy";
    }else if(tram == 8){
        text += "Tám";
    }else if(tram == 9){
        text += "Chín";
    }else{
        text += "Không";
    }

    text = text + " trăm ";
    // Hàng chục
    if(chuc == 1){
        text += "Một";
    }else if(chuc == 2){
        text += "Hai";
    }else if(chuc == 3){
        text += "Ba";
    }else if(chuc == 4){
        text += "Bốn";
    }else if(chuc == 5){
        text += "Năm";
    }else if(chuc == 6){
        text += "Sáu";
    }else if(chuc == 7){
        text += "Bảy";
    }else if(chuc == 8){
        text += "Tám";
    }else if(chuc == 9){
        text += "Chín";
    }else{
        text += "Không";
    }
    text = text + " mươi ";

    // Hàng đơn vị
    if(donVi == 1){
        text += "Một";
    }else if(donVi == 2){
        text += "Hai";
    }else if(donVi == 3){
        text += "Ba";
    }else if(donVi == 4){
        text += "Bốn";
    }else if(donVi == 5){
        text += "Năm";
    }else if(donVi == 6){
        text += "Sáu";
    }else if(donVi == 7){
        text += "Bảy";
    }else if(donVi == 8){
        text += "Tám";
    }else if(donVi == 9){
        text += "Chín";
    }else{
        text += "";
    }
    

    document.getElementById("result").innerHTML = text;
    
}