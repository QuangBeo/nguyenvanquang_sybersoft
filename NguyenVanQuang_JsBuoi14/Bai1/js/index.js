/**
 * 
 * Input:Nhập vào 3 số nguyên
 * 
 * Step:
 * Step1: Tạo 3 biến chứa 3 số nguyên
 * Step2: Tạo biến in ra 3 số theo thứ tự tăng dần
 * Step3: dùng hàm if else và thuật toán để in ra 
 * 
 * 
 *Output: in ra 3 số đó
 */

 function xapXepTangDan(){
    var number1 = document.getElementById('number1').value*1;
    var number2 = document.getElementById('number2').value*1;
    var number3 = document.getElementById('number3').value*1;
    var soft = '';

    if(number1 <= number2 && number1 <= number3) {
        soft +=`${number1} `;
        if(number2 <= number3){
            soft += `${number2} ` + `${number3}`;
        }else{
            soft += `${number3} ` + `${number2}`;
        }
    }
    else if(number2 <= number1 && number2 <= number3) {
        soft += `${number2} `;
        if(number1 <= number3){
            soft += `${number1} ` + `${number3}`;
        }else{
            soft += `${number3} ` + `${number1}`;
        }
    }
    else if(number3 <= number1 && number3 <= number2) {
        soft += `${number3} `;
        if(number1 <= number2){
            soft += `${number1} ` + `${number2}`;
        }else{
            soft += `${number2} ` + `${number1}`;
        }
    }
    document.getElementById("soft").innerHTML = 
    `Dãy số sau khi sấp xếp là: <br> <h3>${soft}</h3>`;
 }
